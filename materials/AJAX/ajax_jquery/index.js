var root = 'https://jsonplaceholder.typicode.com';
var $poemsDiv = $('#poemsDiv');

$.ajax({
    type: 'GET',
    url: root + '/posts',
    success: function(poems){
        console.log('success! got:', poems);
        $.each(poems, function(i, poem){
            $poemsDiv.append('<h4>' + poem.title + '</h4>'+'<p>' + poem.body + '</p>');
        });
    },
    error: function(){
        alert('didnt get my posts');
    }
});





$('#add-poem').on('click', function() {
    var $title = $('#title');
    var $body = $('#body');
    var poem = {
        title: $title.val(),
        body: $body.val(),
        // userId: 99
    };
    $.ajax({
        type: 'POST',
        url: root + '/posts',
        data: poem,
        success: function(newPoem){
            $poemsDiv.append('<h4>'+ newPoem.title + '</h4>' + '<p>' + newPoem.body + '</p>');
        },
        error: function(){
            alert('error posting poem');
        },
    });
    console.log("POSTED ", poem);
});
