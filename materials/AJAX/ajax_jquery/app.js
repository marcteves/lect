const express = require('express');
const app = express();
var path = require('path');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// app.use(express.static(''));
app.get('/', function(req, res){
    res.sendFile(path.join(__dirname + '/index.html'));

});
app.get('/css/bootstrap.min.css', function(req, res){
    res.sendFile(path.join(__dirname + '/css/bootstrap.min.css'));

});
app.get('/js/jquery-3.2.1.js', function(req, res){
    res.sendFile(path.join(__dirname + '/js/jquery-3.2.1.js'));

});


const orders = {
    url: '/orders.json',
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
    }
};

app.get('/api/orders', function(req, res)  {
    console.log('Sent the orders!');
    request(orders, function(err, res, body) {
    var json = JSON.parse(orders);
    console.log(json);
    });
    res.send(request.json);
});





// app.get('/css/bootstrap.min.css', function(req, res){
//     res.sendFile(path.join(__dirname + '/css/bootstrap.min.css'));
//
// });

app.listen(3000, () => console.log('Sean\'s server listening on port 3000!'));
