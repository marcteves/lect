Required program: git
Required commands: git checkout, git clone, git branch


## SETUP
1. Open your terminal with git installed
2. Navigate to a folder where you want the directory containing the repo to be created
3. Enter the command `git clone https://gitlab.com/marcteves/lect.git`
4. Enter the command `cd lect`
5. If you want to use git commands on the `lect` repo, enter them while this is your working directory (the directory your terminal is currently inside of)

## MOVING BETWEEN BRANCHES
1. Enter the command `git branch`. This will display all the branches in the repository. This one contains `master` and `solution`.
2. Navigate to the branch you want with `git checkout <branch name>`
3. Note that almost every git command applies only to the branch you are in.

## Part 1: Using JQuery to select elements, and modify them
1. Navigate to the `master` branch.
2. Edit `script.js` to support the following features.
	1. Add an on-click event handler to the `Add Member` button. Steps 2-5
	are inside this event handler.
	2. Make this event handler read the input from the text input above it 
	and store that input in a variable.
	3. Generate a random Philippine mobile number and return it to another variable. 
	4. With the help of that variable and number, create a new card on the right panel.
	5. (Optional) add animations to make the transition smoother.

## Part 2: JQuery, JSON, and AJAX Exercise
1. Navigate to `json` branch.
2. Rewrite the event handler in `script.js`
	1. Create a JSON object
	2. Update that JSON object with the values found in the input form.
	3. Send the JSON object over the internet with an HTTP POST request using AJAX
	4. Receive the server`s reply and update the webpage accordingly
